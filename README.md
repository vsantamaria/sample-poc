# [ClientName] - Proof of Concept

Multilabel classification model for ClientName Proof of Concept. 
Code, data and documentation 


## Description

See files in docs/ directory for a full description of the PoC requirements.


## Install

Software you need to install before running the code in your machine

```
List here
```

### Required libraries

Libraries, packages and dependencies required to build/run this project. 
Package manager instructions 

```
Instructions here
```

## Authors

Team Name, Company Name


